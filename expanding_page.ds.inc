<?php
/**
 * @file
 * expanding_page.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function expanding_page_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'field_collection_item|field_expanding_areas|default';
  $ds_fieldsetting->entity_type = 'field_collection_item';
  $ds_fieldsetting->bundle = 'field_expanding_areas';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'expand' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['field_collection_item|field_expanding_areas|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function expanding_page_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'expand';
  $ds_field->label = 'Expand';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<a href="#expanded-content-[field_collection_item:item-id]" role="button" class="expand-trigger" title="Click to reveal information below"><h3>[field_collection_item:field_expanding_heading]</h3>
<span class="element-invisible">Click or enter to reveal information below</span> <span class="element-invisible" style="display:none;">Click or enter to hide information below</span></a>
<div class="expand-field"><a name="expanded-content-[field_collection_item:item-id]"></a>[field_collection_item:field_expanding_content]</div>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['expand'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function expanding_page_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_expanding_areas|default';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_expanding_areas';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'expand',
      ),
    ),
    'fields' => array(
      'expand' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_expanding_areas|default'] = $ds_layout;

  return $export;
}
