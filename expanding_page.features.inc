<?php
/**
 * @file
 * expanding_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function expanding_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function expanding_page_node_info() {
  $items = array(
    'expanding_page' => array(
      'name' => t('Expanding Page'),
      'base' => 'node_content',
      'description' => t('Use an <em>expanding page</em> to create a page with sections that expand when a user clicks on the headings—useful for FAQ displays.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
