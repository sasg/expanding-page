<?php
/**
 * @file
 * expanding_page.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function expanding_page_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'field_collection_item-field_expanding_areas-field_expanding_content'
  $field_instances['field_collection_item-field_expanding_areas-field_expanding_content'] = array(
    'bundle' => 'field_expanding_areas',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Content which will be revealed when the user clicks the heading above it.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_expanding_content',
    'label' => 'Expanded Content',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 33,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_expanding_areas-field_expanding_heading'
  $field_instances['field_collection_item-field_expanding_areas-field_expanding_heading'] = array(
    'bundle' => 'field_expanding_areas',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Heading user will click to reveal the content below it.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_expanding_heading',
    'label' => 'Heading trigger',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-expanding_page-field_expanding_areas'
  $field_instances['node-expanding_page-field_expanding_areas'] = array(
    'bundle' => 'expanding_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Add',
          'delete' => 'Delete',
          'description' => TRUE,
          'edit' => 'Edit',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_view',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_expanding_areas',
    'label' => 'Expanding content',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-expanding_page-field_expanding_conclusion'
  $field_instances['node-expanding_page-field_expanding_conclusion'] = array(
    'bundle' => 'expanding_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Place copy here in case there is a need for concluding text after the expanding content areas.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_expanding_conclusion',
    'label' => 'Concluding text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-expanding_page-field_expanding_intro'
  $field_instances['node-expanding_page-field_expanding_intro'] = array(
    'bundle' => 'expanding_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Place copy here in case there is a need for an introduction before the expanding content areas.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_expanding_intro',
    'label' => 'Introductory text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Concluding text');
  t('Content which will be revealed when the user clicks the heading above it.');
  t('Expanded Content');
  t('Expanding content');
  t('Heading trigger');
  t('Heading user will click to reveal the content below it.');
  t('Introductory text');
  t('Place copy here in case there is a need for an introduction before the expanding content areas.');
  t('Place copy here in case there is a need for concluding text after the expanding content areas.');

  return $field_instances;
}
